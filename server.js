// load the things we need
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const fs = require('fs');

// Use assets (css mainly)
app.use(express.static(__dirname + '/assets'));

// set the view engine to ejs
app.set('view engine', 'ejs');

// app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
let rawdataBooks = fs.readFileSync('data/books_db.json');
let bookDB = JSON.parse(rawdataBooks);

let rawdataCd = fs.readFileSync('data/cd_db.json');
let cdDB = JSON.parse(rawdataCd);

// index page
app.get('/', (req, res) => {
    res.render('pages/index');
});

// books page
app.get('/books', (req, res) => {
    res.render('pages/books', {
        bookDB: bookDB.books
    });
});

// book detail page
app.get('/book/:id', (req, res) => {
    res.render('pages/book', {
        book: bookDB.books.find(book => book.id == req.params.id)
    });
});

// cd page
app.get('/cd', (req, res) => {
    res.render('pages/cd', {
        cdDB: cdDB.cds
    });
});

// cd detail page
app.get('/cd/:id', (req, res) => {
    res.render('pages/cd', {
        cd: cdDB.cds.find(cd => cd.id == req.params.id)
    });
});

// about page
app.get('/about', (req, res) => {
    res.render('pages/about');
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});